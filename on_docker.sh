#!/bin/bash

cd /home/$LOCAL_USER_NAME/

cd voxl-scons-3.0.1
python setup.py install
cd ../ComputeLibrary
scons Werror=0 -j16 debug=0 neon=1 opencl=1 os=linux arch=armv7a build=native extra_cxx_flags="-mfloat-abi=softfp"
export LD_LIBRARY_PATH=/home/$LOCAL_USER_NAME/ComputeLibrary/build
build/examples/neon_cnn

# TAR the output
if [ $? -eq 0 ] 
then 
  echo "Tar'ing project"
  cd build/
  tar -cvf armcl.tar *.so
else
  echo "ERROR: ARM Compute Library did not build properly"
fi
