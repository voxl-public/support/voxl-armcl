# Overview
---------------------
ARM Compute Library is an optimized set of routines for ARM processors. It can accelerate many computer vision and deep learning tasks. More details found [here](https://www.arm.com/why-arm/technologies/compute-library)

# Build Arm Compute Library for VOXL
---------------------
1. Requires the voxl-emulator (found [here](https://gitlab.com/voxl-public/voxl-docker)) to run docker ARM image
2. Clone this project
```
$ git clone git@gitlab.com:voxl-public/voxl-armcl.git
$ cd voxl-armcl
```
3. Clone dependencies
```
$ git clone https://github.com/ARM-software/ComputeLibrary.git
$ git clone git@gitlab.com:voxl-public/voxl-scons-3.0.1.git
```
    
4. Build the libraries
```
$ voxl-docker
$ ./on_docker.sh

```

Note: this is built with opencl=1 by default. With opencl=1 the build can take hours. If you just want to test that it's working, edit on_docker.sh to change opencl=0 and the build should take less than an hour.

5. Package as IPK (run within voxl-docker)
```
$ ./package_ipk.sh
```

# Examples

## Alexnet

ARM has a nice example for how to run on Raspberry Pi. This example largely works.
1. Build as above
2. Follow the rest of the [Raspberry Pi Example](https://developer.arm.com/solutions/machine-learning-on-arm/developer-material/how-to-guides/profiling-alexnet-on-raspberry-pi-and-hikey-960-with-the-compute-library)
3. Replace execution line from example with the following
```./graph_alexnet 0 --data=$PATH_ASSETS --image=$PATH_ASSETS/go_kart.ppm --labels=$PATH_ASSETS/labels.txt --target=CL --type=F32```

* [Raspberry Pi Example](https://developer.arm.com/solutions/machine-learning-on-arm/developer-material/how-to-guides/profiling-alexnet-on-raspberry-pi-and-hikey-960-with-the-compute-library)
* [Link to example resources](https://developer.arm.com//-/media/developer/technologies/Machine%20learning%20on%20Arm/Tutorials/Running%20AlexNet%20on%20Pi%20with%20Compute%20Library/compute_library_alexnet.zip?revision=c1a232fa-f328-451f-9bd6-250b83511e01)

### Benchmarking Alexnet
The following modifications to ComputeLibrary/examples/graph_alexnet.cpp enable some high-level benchmarking
```
void do_run() override
{
    // Run graph
    static const int count = 100;
    std::chrono::system_clock::time_point start = std::chrono::system_clock::now();
    for( int i = 0;i<count;i++)
    {
        graph.run();
    }

    std::chrono::system_clock::time_point stop = std::chrono::system_clock::now();
    auto delta = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
    std::cout << "time: " << delta.count() << std::endl;
    std::cout << "time / iteration: " << delta.count()/count << std::endl;
}
```

Results:

32-bit
* --target=NEON --threads=1 =>93.4ms/iteration
* --target=NEON --threads=4 =>69.0ms/iteration
* --target=CL --threads=1 =>49.9ms/iteration
* --target=CL --threads=4 =>50.1ms/iteration

64-bit
* --target=NEON --threads=1 =>70.5ms/iteration
* --target=NEON --threads=2 =>43.8ms/iteration
* --target=NEON --threads=3 =>39.2ms/iteration
* --target=NEON --threads=4 =>34.6ms/iteration



# References

* [OpenCL 1.2 Macros](https://www.khronos.org/registry/OpenCL/sdk/1.2/docs/man/xhtml/macroLimits.html)

Copyright (c) 2019 ModalAI Inc.


