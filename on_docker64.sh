#!/bin/bash

# This is to run on the voxl-cross64 Docker
cd /opt/data/workspace/

# Setup compilers for scons
ln -s /usr/bin/aarch64-linux-gnu-g++-4.9 /usr/bin/aarch64-linux-gnu-g++
ln -s /usr/bin/aarch64-linux-gnu-gcc-4.9 /usr/bin/aarch64-linux-gnu-gcc
ln -s /usr/bin/aarch64-linux-gnu-cpp-4.9 /usr/bin/aarch64-linux-gnu-cpp
ln -s /usr/bin/aarch64-linux-gnu-gcc-ranlib-4.9 /usr/bin/aarch64-linux-gnu-gcc-ranlib
ln -s /usr/bin/aarch64-linux-gnu-gcc-nm-4.9 /usr/bin/aarch64-linux-gnu-gcc-nm
ln -s /usr/bin/aarch64-linux-gnu-gcc-ar-4.9 /usr/bin/aarch64-linux-gnu-gcc-ar
ln -s /usr/bin/aarch64-linux-gnu-gcov-4.9 /usr/bin/aarch64-linux-gnu-gcov

cd voxl-scons-3.0.1
python setup.py install
cd ../ComputeLibrary
scons Werror=1 debug=0 asserts=0 neon=1 opencl=1 examples=1 os=linux arch=arm64-v8a -j4
export LD_LIBRARY_PATH=/home/$LOCAL_USER_NAME/ComputeLibrary/build
build/examples/neon_cnn

